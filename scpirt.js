// 1. Опишіть своїми словами як працює метод forEach.
// Метод для работы с масивами , позволяет использовать заданую функцию один раз для каждого елемента масива , перебирая его елементы
// 2. Як очистити масив?
// * Через [] просто присвоить переменной , которая представляет масив
// * Установить свойство lenght масиву на 0
// * Через splice() указавший 0 как второй аргумент
// 3. Як можна перевірити, що та чи інша змінна є масивом?
// * Методом Array.isArray()
// * Методом Object.prototype.toString()
// * Оператором instanceof




function filterBy(arr, typeToFilter) {
    return arr.filter((item) => typeof item !== typeToFilter);
}

const data = ['hello', 'world', 23, '23', null];
const typeToExclude = 'string';

const filteredData = filterBy(data, typeToExclude);
console.log(filteredData);
